const express   = require('express')
const mongoose  = require('mongoose');
require('dotenv/config');
const app = express()
const port = 3000

//routes
const articlesRouter  = require('./routes/articles');

app.use(express.json());
app.use('/articles', articlesRouter);
// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })

//connect db
mongoose
  .connect(process.env.DB_CONNECTION,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("DB connected"))
  .catch((err) => console.log(err));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})