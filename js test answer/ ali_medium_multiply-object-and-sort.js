// Question
// Given a object data, return the data multiple by 3 and sort the data.
// Expected output : { j: 0, k: 9, i: 18, l: 36 }

const data = { i: 6, j: null, k: 3, l: 12 };

function result(data) {
    //multiplying
    for(var key in data){
        data[key] *= 3
    }
    
    //sorting
    const ordered = Object.keys(data).sort().reduce(
      (obj, key) => { 
        obj[key] = data[key]; 
        return obj;
      }, 
      {}
    );
    
  return ordered
}

console.log(result(data));