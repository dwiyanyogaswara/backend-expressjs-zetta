// Import mongoose
const mongoose = require("mongoose");

const ArticleSchema = mongoose.Schema({
    id: {
        type:String
    },
    title: {
        type:String
    },
    content: {
        type:String
    }
});

module.exports = mongoose.model("articleModel", ArticleSchema);