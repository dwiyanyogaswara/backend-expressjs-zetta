const express   = require("express");
const router    = express.Router();

const articleModel = require("../models/articleModel");

//get all articles
router.get("/", (req, res) => {
    articleModel
    .find()
    .then((resp) => res.status(200).json(resp))
    .catch((err) => res.status(400).json("Request failed!"));
});

//get specific articles
router.get("/:id", (req, res) => {
    articleModel
    .findById(req.params.id)
    .then((resp) => res.status(200).json(resp))
    .catch((err) => res.status(400).json("Request failed!"));
});

module.exports = router;